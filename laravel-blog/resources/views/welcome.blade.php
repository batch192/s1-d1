    @extends('layouts.app')

    @section('content')

        <div style="background-color:#ff3c2b" align="center">
            <img src="https://codebrisk.com/assets/images/posts/1625808145_laravel-banner1.webp" alt="LaravelBanner" width="500" height="200">
        </div>

        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    </div>
                </div>
            @endforeach
        @else
            <div>
                <h2>There are no posts to show</h2>
                <a href="/posts/create" class="btn btn-info">Create post</a>
            </div>
        @endif
    @endsection


{{-- 


Sir Jino's Solution

        @extends('layouts.app')

        @section('content')
            <div class="text-center">
                <img class="w-100" src="https://laravelnews.imgix.net/images/laravel-featured.png">
                <h2 class="my-4">Featured Posts:</h2>
                @if(count($posts) > 0)
                    @foreach($posts as $post)
                        <div class="card text-center">
                            <h4 class="card-title mb-3">
                                <a href="/posts/{{$post->id}}">{{$post->title}}</a>
                            </h4>
                            <h5 class="card-text mb-3">Author: {{$post->user->name}}</h5>                 
                        </div>
                    @endforeach
                @else
                    <div>
                        <h2>There are no posts to show</h2>
                    </div>
                @endif
            </div>
        @endsection


 --}}

